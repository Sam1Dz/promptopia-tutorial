import Mongoose from "mongoose";

const PromptSchema = new Mongoose.Schema({
  creator: {
    type: Mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  prompt: {
    type: String,
    required: [true, "Prompt is required."],
  },
  tag: {
    type: String,
    required: [true, "Tag is required."],
  },
});

const Prompt = Mongoose.models.Prompt || Mongoose.model("Prompt", PromptSchema);

export default Prompt;
