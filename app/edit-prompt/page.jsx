"use client";

import React from "react";
import { useRouter, useSearchParams } from "next/navigation";

import { useSession } from "next-auth/react";

import Form from "@components/Form";

export default function UpdatePromptPage() {
  const NextRouter = useRouter();
  const NextSearchParams = useSearchParams();
  const { data: session } = useSession();

  const promptId = NextSearchParams.get("id");

  const [submitting, setSubmitting] = React.useState(false);
  const [post, setPost] = React.useState({
    prompt: "",
    tag: "",
  });

  const editPrompt = async (event) => {
    event.preventDefault();
    setSubmitting(true);

    if (!promptId) return alert("Prompt ID not found");

    try {
      const response = await fetch(`/api/prompt/${promptId}`, {
        method: "PATCH",
        body: JSON.stringify({
          prompt: post.prompt,
          tag: post.tag,
        }),
      });

      if (response.ok) {
        NextRouter.push("/profile");
      }
    } catch (error) {
      console.error(error);
    } finally {
      setSubmitting(false);
    }
  };

  React.useEffect(() => {
    const getPromptDetails = async () => {
      const response = await fetch(`/api/prompt/${promptId}`);
      const data = await response.json();

      setPost({
        prompt: data.prompt,
        tag: data.tag,
      });
    };

    if (promptId) getPromptDetails();
  }, [promptId]);

  return (
    <Form
      type="Edit"
      post={post}
      setPost={setPost}
      submitting={submitting}
      handleSubmit={editPrompt}
    />
  );
}
