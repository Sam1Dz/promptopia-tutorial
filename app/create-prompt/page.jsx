"use client";

import React from "react";
import { useRouter } from "next/navigation";

import { useSession } from "next-auth/react";

import Form from "@components/Form";

export default function CreatePromptPage() {
  const NextRouter = useRouter();
  const { data: session } = useSession();

  const [submitting, setSubmitting] = React.useState(false);
  const [post, setPost] = React.useState({
    prompt: "",
    tag: "",
  });

  const createPrompt = async (event) => {
    event.preventDefault();
    setSubmitting(true);

    try {
      const response = await fetch("/api/prompt/new", {
        method: "POST",
        body: JSON.stringify({
          prompt: post.prompt,
          userId: session?.user.id,
          tag: post.tag,
        }),
      });

      if (response.ok) {
        NextRouter.push("/");
      }
    } catch (error) {
      console.error(error);
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <Form
      type="Create"
      post={post}
      setPost={setPost}
      submitting={submitting}
      handleSubmit={createPrompt}
    />
  );
}
