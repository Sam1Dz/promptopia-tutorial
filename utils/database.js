import Mongoose from "mongoose";

let isConnected = false; // track the connection

export const ConnectDB = async () => {
  Mongoose.set("strictQuery", true);

  if (isConnected) {
    console.info("MongoDB is already connected");
    return;
  }

  try {
    await Mongoose.connect(process.env.MONGODB_URL, {
      dbName: "promptopia",
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    isConnected = true;
    console.info("MongoDB connected");
  } catch (error) {
    console.error(error);
  }
};
